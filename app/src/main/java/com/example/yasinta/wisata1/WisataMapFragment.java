package com.example.yasinta.wisata1;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;


/**
 * A simple {@link Fragment} subclass.
 */
public class WisataMapFragment extends Fragment {


    public WisataMapFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_wisata_map, container, false);
        mapFrag = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.mapViewWisata);
        mapFrag.getMapAsync(this);

        return view;
    }


    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        // Add a marker in Sydney and move the camera
        LatLng semarang = new LatLng(-6.966667, 110.416664);
        mMap.addMarker(new MarkerOptions().position(semarang).title("Marker in Semarang"));

        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(semarang,10));

    }

    }


