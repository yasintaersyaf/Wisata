package com.example.yasinta.wisata1.rest;

import com.example.yasinta.wisata1.ListWisataModel;

import retrofit2.Call;
import retrofit2.http.GET;

public interface ApiServices {
    @GET("getwisata.php")
    Call<ListWisataModel> ambilDataWisata();
}
