package com.example.yasinta.wisata1;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;

public class WisataAdapter extends
        RecyclerView.Adapter<WisataAdapter.MyViewHolder> {
    private Context context;
    private ArrayList<WisataModel> listData;

    //generate constructor
    public WisataAdapter(Context context, ArrayList<WisataModel> listData) {
        this.context = context;
        this.listData = listData;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        //menghubungkan dengan movie_item
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.wisata_item_list, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        //ngapain
        //set data
        Glide.with(context)       .load("http://52.187.117.60/wisata_semarang/img/wisata/"+listData.get(position).getGambarWisata())
                .placeholder(R.drawable.no_image_found)
                .error(R.drawable.no_image_found)
                .into(holder.ivItemGambar);
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent pindah = new Intent(context, DetailWisataActivity.class);
                Bundle data = new Bundle();
                data.putString(Konstanta.DATA_ID, listData.get(position).getIdWisata());
                data.putString(Konstanta.DATA_NAMA, listData.get(position).getNamaWisata());
                data.putString(Konstanta.DATA_GAMBAR, listData.get(position).getGambarWisata());
                data.putString(Konstanta.DATA_DESKRIPSI, listData.get(position).getDeksripsiWisata());
                data.putString(Konstanta.DATA_ALAMAT, listData.get(position).getAlamatWisata());
                data.putString(Konstanta.DATA_LAT, listData.get(position).getLatitudeWisata());
                data.putString(Konstanta.DATA_LNG, listData.get(position).getLongitudeWisata());
                pindah.putExtras(data);
                context.startActivity(pindah);
            }
        });

    }

    @Override
    public int getItemCount() {
        //jumlah list
        return listData.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        //menyambungkan dengan komponen yg di dalam item
        ImageView ivItemGambar;
        TextView tvItemNama;
        TextView tvItemAlamat;
        public MyViewHolder(View itemView) {
            super(itemView);
            ivItemGambar = (ImageView) itemView.findViewById(R.id.iv_item_gambar);
            tvItemNama = (TextView) itemView.findViewById(R.id.tv_item_nama);
            tvItemAlamat = (TextView) itemView.findViewById(R.id.tv_item_alamat);
        }
    }
}


