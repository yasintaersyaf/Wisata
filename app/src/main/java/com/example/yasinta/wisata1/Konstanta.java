package com.example.yasinta.wisata1;

class Konstanta {
    public static final String DATA_ID = "id";
    public static final String DATA_NAMA = "nama";
    public static final String DATA_GAMBAR = "gambar";
    public static final String DATA_DESKRIPSI = "deskripsi";
    public static final String DATA_ALAMAT = "alamat";
    public static final String DATA_LAT = "lat";
    public static final String DATA_LNG = "lng";
}

